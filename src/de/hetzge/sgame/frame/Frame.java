package de.hetzge.sgame.frame;

import java.util.Set;
import java.util.TreeSet;

public final class Frame {

	private final int id;
	private final Set<IF_FrameEvent> frameEvents;
	private Frame next;

	public Frame(int id) {
		this.id = id;
		this.frameEvents = new TreeSet<>(new IF_FrameEvent.TimestampComparator());
	}

	public Frame next() {
		if (this.next == null) {
			this.next = new Frame(this.id + 1);
		}
		return this.next;
	}

	public void addFrameEvent(IF_FrameEvent frameEvent) {
		int frameId = frameEvent.getFrameId();
		if (frameId < this.id) {
			if (frameId == 0) {
				throw new IllegalStateException("frame event with unknown frame id: " + frameEvent);
			} else {
				throw new IllegalStateException("frame event reached to late");
			}
		}
		if (frameId == this.id) {
			this.frameEvents.add(frameEvent);
		} else {
			next().addFrameEvent(frameEvent);
		}
	}

	public void execute() {
		for (IF_FrameEvent frameEvent : this.frameEvents) {
			frameEvent.execute();
		}
	}

	public int getId() {
		return this.id;
	}

}
