package de.hetzge.sgame.frame;

public final class Timer {

	/**
	 * <code>true</code> if timer is started.
	 */
	private boolean started = false;

	/**
	 * Every <code>everyMilliseconds</code> a frame will/should happen.
	 */
	private final short everyMilliseconds;

	/**
	 * The timestamp when the last frame happened. If timer not never runned
	 * before this value will be 0.
	 */
	private long timestamp = 0L;

	/**
	 * {@link Runnable} which should be executed each frame.
	 */
	private final Runnable execute;

	public Timer(short fps, Runnable execute) {
		this.execute = execute;
		this.everyMilliseconds = (short) (1000 / fps);
	}

	public void call() {
		if (this.started) {
			long currentTimeMillis = System.currentTimeMillis();
			if (currentTimeMillis > this.timestamp + this.everyMilliseconds) {
				this.timestamp = this.timestamp == 0L ? currentTimeMillis : this.timestamp + this.everyMilliseconds;
				this.execute.run();
			}
		}
	}

	public short getEveryMilliseconds() {
		return this.everyMilliseconds;
	}

	public long getTimestamp() {
		return this.timestamp;
	}

	public boolean isStarted() {
		return this.started;
	}

	public void start() {
		this.started = true;
		this.timestamp = 0;
	}

	public void stop() {
		this.started = false;
	}

	public float delta() {
		return (float) (System.currentTimeMillis() - this.timestamp) / this.everyMilliseconds;
	}

}
