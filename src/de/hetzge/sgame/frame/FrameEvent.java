package de.hetzge.sgame.frame;

public abstract class FrameEvent implements IF_FrameEvent {

	private final long timestamp;
	private final int frameId;

	public FrameEvent(int frameId) {
		this.frameId = frameId;
		this.timestamp = System.currentTimeMillis();
	}

	@Override
	public long getTimestamp() {
		return this.timestamp;
	}

	@Override
	public int getFrameId() {
		return this.frameId;
	}

}
