package de.hetzge.sgame.frame;

import org.pmw.tinylog.Logger;

public final class Timeline {

	public static final short FRAMES_PER_SECOND = 30;
	public static final short DEFAULT_NEXT_FRAME_OFFSET = FRAMES_PER_SECOND;
	public static final float FRAME_DELTA = 1f / FRAMES_PER_SECOND;

	private final Timer frameTimer;
	private final Runnable frameCallback;

	private Frame current = new Frame(0);

	public Timeline(Runnable frameCallback) {
		this.frameCallback = frameCallback;
		this.frameTimer = new Timer(FRAMES_PER_SECOND, this::onFrame);
	}

	public void addFrameEvent(IF_FrameEvent frameEvent) {
		this.current.addFrameEvent(frameEvent);
	}

	private void onFrame() {
		long before = System.currentTimeMillis();
		this.current.execute();
		this.frameCallback.run();
		long executionTime = System.currentTimeMillis() - before;
		if (executionTime > this.frameTimer.getEveryMilliseconds()) {
			Logger.warn("frame executiontime is longer then frametime: " + executionTime);
		}
		this.current = this.current.next();
	}

	public void update() {
		Util.sleep(10);
		this.frameTimer.call();
	}

	public void startFrameTimer() {
		this.frameTimer.start();
	}

	public void stopFrameTimer() {
		this.frameTimer.stop();
	}

	public boolean isCurrentOrPast(int frameId) {
		return getCurrentFrameId() >= frameId;
	}

	public int getCurrentFrameId() {
		return this.current.getId();
	}

	public boolean isXthFrame(int xth) {
		return getCurrentFrameId() % xth == 0;
	}

	public int getNextFrameId(int frames) {
		return getCurrentFrameId() + frames;
	}

	public int getDefaultNextFrameId() {
		return getCurrentFrameId() + DEFAULT_NEXT_FRAME_OFFSET;
	}

	public float getFrameDelta() {
		return FRAME_DELTA;
	}
}
