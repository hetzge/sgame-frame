package de.hetzge.sgame.frame;

import java.io.Serializable;
import java.util.Comparator;

public interface IF_FrameEvent extends Serializable {

	long getTimestamp();

	int getFrameId();

	void execute();

	public static class TimestampComparator implements Comparator<IF_FrameEvent>, Serializable {

		@Override
		public int compare(IF_FrameEvent a, IF_FrameEvent b) {
			long timestampA = a.getTimestamp();
			long timestampB = b.getTimestamp();
			if (timestampA == timestampB && a != b) {
				// TODO temp solution
				throw new IllegalStateException("frame event collision (two frame events with same timestamp");
			} else {
				return Long.compare(timestampA, timestampB);
			}
		}

	}

}
